package cavar.d42.router

import androidx.fragment.app.Fragment
import cavar.d42.app_commons.router.Router
import cavar.d42.app_home.home.ui.HomeFragment
import cavar.d42.app_map.ui.MapFragment
import cavar.d42.app_splash.ui.SplashFragment

class AppRouter : Router {

    override fun createSplashFragment(): Fragment {
        return SplashFragment.newInstance()
    }

    override fun createHomeFragment(): Fragment {
        return HomeFragment.newInstance()
    }

    override fun createMapFragment(): Fragment {
        return MapFragment.newInstance()
    }
}