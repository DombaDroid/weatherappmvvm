package cavar.d42.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cavar.d42.R
import cavar.d42.app_commons.router.Router
import cavar.d42.app_splash.ui.SplashFragment
import org.koin.android.ext.android.get
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val router: Router by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportFragmentManager.findFragmentById(android.R.id.content) == null) {
            val fragmeManager = supportFragmentManager
            val fragmentTransaction = fragmeManager?.beginTransaction()
            fragmentTransaction?.replace(android.R.id.content, router.createSplashFragment(), SplashFragment.TAG)
            fragmentTransaction?.commit()
        }
    }
}
