package cavar.d42

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import cavar.d42.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                appModule, locationService, interactorModule, serviceWeatherApiModule, serviceAutoCompleteApiModule,
                splashModule, homeModule, mapModule
            )
        }
    }
}