package cavar.d42.di

import android.content.Context
import android.view.inputmethod.InputMethodManager
import cavar.d42.api_common.AutoCompleteRestInterface
import cavar.d42.api_common.WeatherRestInterface
import cavar.d42.api_common.interactors.auto_complete.AutoCompleteInteractor
import cavar.d42.api_common.interactors.auto_complete.AutoCompleteInteractorImpl
import cavar.d42.api_common.interactors.location.LocationInteractor
import cavar.d42.api_common.interactors.location.LocationInteractorImpl
import cavar.d42.api_common.interactors.weather.WeatherInteractor
import cavar.d42.api_common.interactors.weather.WeatherInteractorImpl
import cavar.d42.app_api.services.auto_complete.createAutoCompleteWebService
import cavar.d42.app_api.services.weather.createWeatherWebService
import cavar.d42.app_commons.router.Router
import cavar.d42.app_home.home.repository.HomeRepository
import cavar.d42.app_home.home.ui.HomeViewModel
import cavar.d42.app_map.repository.MapRepository
import cavar.d42.app_map.ui.MapViewModel
import cavar.d42.app_splash.repository.SplashRepository
import cavar.d42.app_splash.ui.SplashViewModel
import cavar.d42.error_handler.ErrorHandler
import cavar.d42.router.AppRouter
import d42.cavar.app_location_api.service.location.provideLocationService
import d42.cavar.app_location_api.service.location.provideRxLocation
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { AppRouter() as Router }
    single { androidContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager }
    single { androidContext().resources }
    single {ErrorHandler(get())}
}

val locationService = module {
    single { provideRxLocation(androidApplication()) }
    single { provideLocationService() }
}

val interactorModule = module {
    factory { LocationInteractorImpl(get(), get()) as LocationInteractor }
    factory { WeatherInteractorImpl(get()) as WeatherInteractor }
    factory { AutoCompleteInteractorImpl(get()) as AutoCompleteInteractor }
}

val serviceWeatherApiModule = module {
    single { createWeatherWebService<WeatherRestInterface>() }
}

val serviceAutoCompleteApiModule = module {
    single { createAutoCompleteWebService<AutoCompleteRestInterface>() }
}

val splashModule = module {
    factory { SplashRepository(get(), get(), get()) }
    viewModel { SplashViewModel(get()) }
}

val homeModule = module {
    factory { HomeRepository(get(), get(), get()) }
    viewModel { HomeViewModel(get()) }
}

val mapModule = module {
    factory { MapRepository(get()) }
    viewModel { MapViewModel(get()) }
}