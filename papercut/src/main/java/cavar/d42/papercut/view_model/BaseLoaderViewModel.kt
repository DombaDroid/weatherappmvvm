package cavar.d42.papercut.view_model

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

abstract class BaseLoaderViewModel : BaseViewModel(), BaseLoader {

    private val _isLoading = MutableLiveData<Boolean>(false)

    val isLoading: LiveData<Boolean>
        get() = _isLoading

    override fun isLoading(isLoading: Boolean) {
        _isLoading.value = isLoading
    }

    fun <T> PublishSubject<T>.publish(t: T) {
        //isLoading(true)
        onNext(t)
    }

    fun <T> PublishSubject<T>.onNextWithLoader(t: T) {
        isLoading(true)
        onNext(t)
    }

    fun <T> Observable<T>.withLoader(): Observable<T> {
        return doOnError {
            isLoading(false)
        }
            .doOnNext { isLoading(false) }
    }
}

interface BaseLoader {
    fun isLoading(isLoading: Boolean)
}