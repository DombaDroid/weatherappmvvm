package cavar.d42.papercut.view_model

import androidx.lifecycle.LiveData
import cavar.d42.error_handler.ErrorResult
import cavar.d42.error_handler.RefreshListener
import cavar.d42.error_handler.models.ErrorLayout
import cavar.d42.error_handler.models.ErrorSnack
import cavar.d42.papercut.custom_events.SingleLiveEvent

abstract class BaseErrorViewModel : BaseLoaderViewModel(), ErrorUiHandler, RefreshListener {

    private val _showSnackError = SingleLiveEvent<ErrorSnack>()
    val showSnackError: LiveData<ErrorSnack>
        get() = _showSnackError

    private val _showErrorLayout = SingleLiveEvent<ErrorLayout>()
    val showErrorLayout: LiveData<ErrorLayout>
        get() = _showErrorLayout

    protected val _hideErrorLayout = SingleLiveEvent<Any>()
    val hideErrorLayout: LiveData<Any>
        get() = _hideErrorLayout

    override fun handleError(error: ErrorResult) {
        when (error) {
            is ErrorResult.ErrorSnackData -> {
                _showSnackError.value = error.data
            }
            is ErrorResult.ErrorLayoutData -> {
                val refreshListener: RefreshListener = this
                _showErrorLayout.value = error.data.apply { listener = refreshListener }
            }
        }
    }
}

interface ErrorUiHandler {
    fun handleError(error: ErrorResult)
}