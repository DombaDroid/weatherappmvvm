package cavar.d42.papercut.view_model

import androidx.lifecycle.LiveData
import cavar.d42.papercut.custom_events.SingleLiveEvent
import cavar.d42.papercut.fragment.SnackData

abstract class BaseViewModel : BaseKoinViewModel() {

    protected val _showSnack = SingleLiveEvent<SnackData>()
    val showSnack: LiveData<SnackData>
        get() = _showSnack

    protected val _hideKeyboard = SingleLiveEvent<Any>()
    val hideKeyboard: LiveData<Any>
        get() = _hideKeyboard

    protected val _showKeyboard = SingleLiveEvent<Any>()
    val showKeyboard: LiveData<Any>
        get() = _showKeyboard

    protected val _backPress = SingleLiveEvent<Any>()
    val backPress: LiveData<Any>
        get() = _backPress
}