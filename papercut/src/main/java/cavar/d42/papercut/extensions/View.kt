package cavar.d42.papercut.extensions

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelStoreOwner
import cavar.d42.papercut.R
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.ViewModelParameters
import org.koin.androidx.viewmodel.ViewModelStoreOwnerDefinition
import org.koin.androidx.viewmodel.ext.android.getSharedViewModel
import org.koin.androidx.viewmodel.getViewModel
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.core.scope.Scope
import kotlin.reflect.KClass

inline var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

inline var View.isInvisible: Boolean
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

inline var View.isGone: Boolean
    get() = visibility == View.GONE
    set(value) {
        visibility = if (value) View.GONE else View.VISIBLE
    }

fun Fragment.showFakeSnackBar(string: Any, colorId: Int = R.color.black_cobalt) {
    val layout = LayoutInflater.from(context).inflate(R.layout.snack_layout, null)
    val title = layout.findViewById<TextView>(R.id.tvSnackText)
    val root = layout.findViewById<LinearLayout>(R.id.llSnackContainer)
    root.setBackgroundColor(ContextCompat.getColor(context!!, colorId))
    title.text = getString(string, context!!)
    with(Toast(context)) {
        setGravity(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL, 0, 0)
        duration = Toast.LENGTH_SHORT
        view = layout
        show()
    }
}

private fun getString(string: Any, context: Context): String {
    return when (string) {
        is String -> string
        is Int -> context.resources.getString(string)
        else -> ""
    }
}

inline fun <T : ViewModel> Fragment.getSharedViewModel(
    clazz: KClass<T>,
    qualifier: Qualifier? = null,
    scope: Scope? = null,
    noinline from: ViewModelStoreOwnerDefinition = { activity as ViewModelStoreOwner },
    noinline parameters: ParametersDefinition? = null
): T {
    val koin = getKoin()
    return koin.getViewModel(
        ViewModelParameters(
            clazz,
            this@getSharedViewModel,
            scope ?: koin.defaultScope,
            qualifier,
            from,
            parameters
        )
    )
}