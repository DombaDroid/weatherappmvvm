package cavar.d42.papercut.extensions

import cavar.d42.error_handler.ErrorResult
import cavar.d42.papercut.view_model.BaseLoader
import cavar.d42.papercut.view_model.ErrorUiHandler
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.internal.functions.Functions
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

fun <T> Observable<T>.usualThreads(): Observable<T>{
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread(), true)
}