package cavar.d42.papercut.fragment

import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import cavar.d42.papercut.R
import cavar.d42.papercut.extensions.isVisible
import cavar.d42.papercut.view_model.BaseLoaderViewModel
import kotlinx.android.synthetic.main.layout_loader.*
import kotlin.reflect.KClass

abstract class BaseLoaderFragment<T : BaseLoaderViewModel>(klass: KClass<T>)  : BaseFragment<T>(klass) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.isLoading.observe(this, Observer {
            clLoader?.isVisible = it
        })
    }
}