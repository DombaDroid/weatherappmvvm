package cavar.d42.papercut.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.Observer
import cavar.d42.papercut.view_model.BaseViewModel
import cavar.d42.papercut.R
import cavar.d42.papercut.constants.EMPTY_STRING
import cavar.d42.papercut.extensions.showFakeSnackBar
import org.koin.android.ext.android.inject
import kotlin.reflect.KClass

//User for single events like showing snack bars, dialog pickers, etc.

data class SnackData(var string: Any = EMPTY_STRING, val colorId: Int = R.color.black_cobalt)

abstract class BaseFragment<T : BaseViewModel>(klass: KClass<T>) : BaseKoinFragment<T>(klass) {

    val inputMethodManager by inject<InputMethodManager>()

    final override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.showSnack.observe(this, Observer {
            showFakeSnackBar(it.string, it.colorId)
        })

        viewModel.showKeyboard.observe(this, Observer {
            view?.requestFocus()
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
        })

        viewModel.hideKeyboard.observe(this, Observer {
            activity!!.currentFocus?.let {
                inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
            }
        })

        viewModel.backPress.observe(this, Observer {
            activity?.onBackPressed()
        })
    }
}