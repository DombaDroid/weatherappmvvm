package cavar.d42.papercut.fragment

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import cavar.d42.papercut.extensions.isVisible
import cavar.d42.papercut.extensions.showFakeSnackBar
import cavar.d42.papercut.view_model.BaseErrorViewModel
import kotlinx.android.synthetic.main.error_layout.*
import kotlin.reflect.KClass

abstract class BaseErrorFragment<T : BaseErrorViewModel>(klass: KClass<T>) :
    BaseLoaderFragment<T>(klass) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.showSnackError.observe(this, Observer {
            showFakeSnackBar(it.string, it.colorId)
        })

        viewModel.showErrorLayout.observe(this, Observer { errorData ->
            activity!!.window?.setBackgroundDrawable(null)
            rlErrorRoot?.isVisible = true
            ivErrorIcon.setImageDrawable(ContextCompat.getDrawable(context!!, errorData.imageSrc))
            tvErrorTitle.text = getString(errorData.title)
            tvErrorDescription.text = getString(errorData.description)
            tvRefresh.text = getString(errorData.buttonText)
            tvRefresh?.setOnClickListener { errorData.listener?.onRefresh() }
        })

        viewModel.hideErrorLayout.observe(this, Observer {
            activity!!.window?.setBackgroundDrawable(null)
            rlErrorRoot?.isVisible = false
        })
    }
}