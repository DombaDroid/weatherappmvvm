package cavar.d42.app_splash.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.checkPermission
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import cavar.d42.app_api.constants.REQUEST_FINE_LOCATION
import cavar.d42.app_commons.constants.EXTRAS_WEATHER
import cavar.d42.app_commons.extensions.loadFragmentAndRemoveCurrent
import cavar.d42.app_commons.router.Router
import cavar.d42.app_splash.R
import cavar.d42.papercut.fragment.BaseErrorFragment
import io.reactivex.subjects.PublishSubject
import org.koin.android.ext.android.inject

class SplashFragment : BaseErrorFragment<SplashViewModel>(SplashViewModel::class) {

    companion object {
        const val TAG = "SplashFragment"
        fun newInstance(): Fragment {
            return SplashFragment()
        }
    }

    val router: Router by inject()

    override val layoutRes: Int = R.layout.fragment_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.checkPermission.observe(this, Observer {
            checkPermissions()
        })

        viewModel.loadHomeFragment.observe(this, Observer {
            val bundle = Bundle()
            bundle.putParcelable(EXTRAS_WEATHER, it)
            loadFragmentAndRemoveCurrent(router.createHomeFragment(), bundle)
        })
    }

    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
            )
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_FINE_LOCATION)
            else
                viewModel.onPermission(true)
        } else {
            viewModel.onPermission(true)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_FINE_LOCATION && permissions.isNotEmpty())
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                viewModel.onPermission(true)
            else
                viewModel.onPermission(false)
    }
}