package cavar.d42.app_splash.ui

import androidx.lifecycle.LiveData
import cavar.d42.api_common.models.WeatherCity
import cavar.d42.app_splash.repository.SplashRepository
import cavar.d42.papercut.custom_events.SingleLiveEvent
import cavar.d42.papercut.extensions.usualThreads
import cavar.d42.papercut.fragment.SnackData
import cavar.d42.papercut.view_model.BaseErrorViewModel
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject

class SplashViewModel(val repository: SplashRepository) : BaseErrorViewModel() {

    private val _checkPermissions = SingleLiveEvent<Any>()
    val checkPermission: LiveData<Any>
        get() = _checkPermissions

    private val _loadHomeFragment = SingleLiveEvent<WeatherCity>()
    val loadHomeFragment: LiveData<WeatherCity>
        get() = _loadHomeFragment

    private val eventPermission = PublishSubject.create<Boolean>()

    private var permissionGranted: Boolean = false

    init {
        _checkPermissions.call()

        addDisposable(eventPermission.flatMap {
            if (it)
                repository.getWeatherByCoordinates()
            else
                repository.getWeatherByCityName()
        }.usualThreads()
            .subscribeBy(
                onNext = {
                    it.value?.let {
                        _loadHomeFragment.value = it
                    }
                    handleError(it.error)
                }
            ))
    }

    fun onPermission(isSuccess: Boolean) {
        permissionGranted = isSuccess
        eventPermission.onNext(isSuccess)
    }


    override fun onRefresh() {
        _hideErrorLayout.call()
        eventPermission.onNext(permissionGranted)
    }
}