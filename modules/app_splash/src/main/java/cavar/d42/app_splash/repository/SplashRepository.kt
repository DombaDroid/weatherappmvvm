package cavar.d42.app_splash.repository

import android.location.Location
import cavar.d42.api_common.data.RxWrapper
import cavar.d42.api_common.interactors.location.LocationInteractor
import cavar.d42.api_common.interactors.weather.WeatherInteractor
import cavar.d42.app_api.constants.DEFAULT_CITY
import cavar.d42.app_api.constants.DEFAULT_CITY_LATITUDE
import cavar.d42.app_api.constants.DEFAULT_CITY_LONGITUDE
import cavar.d42.error_handler.ErrorHandler
import cavar.d42.error_handler.ErrorType
import cavar.d42.papercut.constants.EMPTY_STRING
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class SplashRepository(
    private val weatherInteractor: WeatherInteractor,
    private val locationInteractor: LocationInteractor,
    private val errorHandler: ErrorHandler
) {

    fun getWeatherByCoordinates() = locationInteractor.getLatestLocations()
        .take(1)
        .timeout(7, TimeUnit.SECONDS, Observable.defer {
            val defaultLocation = Location(EMPTY_STRING)
            defaultLocation.latitude = DEFAULT_CITY_LATITUDE
            defaultLocation.longitude = DEFAULT_CITY_LONGITUDE
            Observable.just(defaultLocation)
        })
        .flatMap {
            weatherInteractor.getCurrentWeatherByCoordinates(it.latitude, it.longitude)
                .flatMap { Observable.just(RxWrapper(it)) }
                .onErrorReturn {
                    RxWrapper(null, errorHandler.baseHttpErrorHandler(ErrorType.Layout, it))
                }
        }

    fun getWeatherByCityName() = weatherInteractor.getCurrentWeatherByCityName(DEFAULT_CITY)
        .flatMap { Observable.just(RxWrapper(it)) }
        .onErrorReturn {
            RxWrapper(null, errorHandler.baseHttpErrorHandler(ErrorType.Layout, it))
        }
}