package cavar.d42.app_commons.fragment_dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.text.TextUtils
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import cavar.d42.app_commons.constants.EMPTY_STRING
import cavar.d42.app_commons.utils.formatToCalendar
import java.util.*

const val TAG = "DatePickerFragment"

class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    companion object {
        const val EXTRA_TIMESTAMP = "EXTRA_TIMESTAMP"
        const val EXTRA_TYPE_SELECTION = "EXTRA_TYPE_SELECTION"
        const val SELECTION_TYPE_PAST = 0
        const val SELECTION_TYPE_FUTURE = 1
        const val SELECTION_TYPE_ALL = 2

        fun newInstance(timestamp: String = EMPTY_STRING, selectionType: Int = SELECTION_TYPE_ALL): DatePickerFragment {
            val bundle = Bundle()
            bundle.putString(EXTRA_TIMESTAMP, timestamp)
            bundle.putInt(EXTRA_TYPE_SELECTION, selectionType)
            val fragment = DatePickerFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    var delegate: DatePickerDialog.OnDateSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        var year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        var day = c.get(Calendar.DAY_OF_MONTH)

        val timestamp = arguments!!.getString(EXTRA_TIMESTAMP, EMPTY_STRING)
        if (!TextUtils.isEmpty(timestamp)) {
            val prefilledCalendar = formatToCalendar(timestamp)
            year = prefilledCalendar.get(Calendar.YEAR)
            month = prefilledCalendar.get(Calendar.MONTH)
            day = prefilledCalendar.get(Calendar.DAY_OF_MONTH)
        }
        val dialog = DatePickerDialog(context, this, year, month, day)
        when(arguments!!.getInt(EXTRA_TYPE_SELECTION)){
            SELECTION_TYPE_PAST ->  dialog.datePicker.maxDate = System.currentTimeMillis()
            SELECTION_TYPE_FUTURE -> dialog.datePicker.minDate = System.currentTimeMillis()
        }
        return dialog
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        delegate?.onDateSet(view, year, month, dayOfMonth)
    }
}