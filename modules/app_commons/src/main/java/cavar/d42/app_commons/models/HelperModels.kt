package cavar.d42.app_commons.models

class HelperModels

data class Coordinates(val latitude: Double, val longitude: Double)