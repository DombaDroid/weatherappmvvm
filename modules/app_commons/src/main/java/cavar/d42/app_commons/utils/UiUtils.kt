package cavar.d42.app_commons.utils

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.util.TypedValue

fun spToPx(sp: Float, context: Context) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.resources.displayMetrics).toInt()

fun dpToPx(dp: Float, context: Context) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.resources.displayMetrics)

fun getScreenWidth(context: Context): Int {
    val activity: Activity = context as Activity
    val display = activity.windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    return size.x
}