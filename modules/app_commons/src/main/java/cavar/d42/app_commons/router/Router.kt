package cavar.d42.app_commons.router

import androidx.fragment.app.Fragment

interface Router {
    fun createSplashFragment(): Fragment
    fun createHomeFragment(): Fragment
    fun createMapFragment(): Fragment
}