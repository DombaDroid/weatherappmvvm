package cavar.d42.app_commons.constants

//Init constants
const val EMPTY_STRING = ""
const val INIT_INT = -1
const val INVALID_COORDINATE = -99999.99
//EXTRAS
const val EXTRAS_WEATHER = "EXTRAS_WEATHER"
const val EXTRAS_LATITUDE = "EXTRAS_LATITUDE"
const val EXTRAS_LONGITUDE = "EXTRAS_LONGITUDE"
//RESULTS
const val MAP_TO_HOME_RESULT = 10041941