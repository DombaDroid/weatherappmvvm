package cavar.d42.app_commons.extensions

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE
import cavar.d42.app_commons.R
import cavar.d42.app_commons.constants.EMPTY_STRING
import cavar.d42.app_commons.constants.INIT_INT
import cavar.d42.app_commons.constants.MAP_TO_HOME_RESULT
import cavar.d42.app_commons.utils.dpToPx
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

const val EXTRA_RESULT = "EXTRA_RESULT"

fun ImageView.loadImage(url: String) {
    setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_placholder))
    Glide.with(context)
        .asBitmap()
        .load(url)
        .into(this)
}

fun ImageView.loadCorneredImage(url: String) {
    val requestOptions = RequestOptions().transforms(CenterCrop(), RoundedCorners(dpToPx(3F, context).toInt()))
    setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_placholder))
    Glide.with(context)
        .load(url)
        .apply(requestOptions)
        .into(this)
}

inline var TextView.formatText
    get() = { text }.toString()
    set(value) {
        text = if (value.contains(INIT_INT.toString()))
            EMPTY_STRING
        else
            value
    }

fun ImageView.setImageSrc(id: Int) {
    if (id != INIT_INT)
        setImageResource(id)
}

fun CoordinatorLayout.setBackgroundSrc(context: Context, id: Int) {
    if (id != INIT_INT) {
        rootView.background = ContextCompat.getDrawable(context, id)
    }
}

fun String.capitaliseFirstLetter(): String {
    return if (isNotEmpty())
        substring(0, 1).toUpperCase() + substring(1)
    else
        EMPTY_STRING
}

fun Fragment.loadFragmentAndRemoveCurrent(fragment: Fragment, bundle: Bundle? = null) {
    fragmentManager?.let {
        fragment.arguments = bundle
        val addTransaction = it.beginTransaction()
        addTransaction.setTransition(TRANSIT_FRAGMENT_FADE)
        addTransaction.replace(android.R.id.content, fragment, fragment.tag)
        addTransaction.commit()
    }
}

fun Fragment.loadFragmentFoResult(childFragment: Fragment, parentFragment: Fragment, bundle: Bundle? = null) {
    fragmentManager?.let {
        childFragment.setTargetFragment(parentFragment, MAP_TO_HOME_RESULT)
        val addTransaction = it.beginTransaction()
        addTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_in_left)
        addTransaction.add(android.R.id.content, childFragment, childFragment.tag)
        addTransaction.addToBackStack(childFragment.tag)
        addTransaction.commit()
    }
}

fun Fragment.returnFragmentForResult(intent: Intent?) {
    targetFragment?.let {
        it.onActivityResult(targetRequestCode, RESULT_OK, intent)
        fragmentManager?.popBackStack()
    }
}

