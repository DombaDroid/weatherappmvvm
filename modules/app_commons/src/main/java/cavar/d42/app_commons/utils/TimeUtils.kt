package cavar.d42.app_commons.utils

import android.content.Context
import android.icu.lang.UCharacter.LineBreak.SPACE
import cavar.d42.app_commons.R
import cavar.d42.app_commons.constants.EMPTY_STRING
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

const val TIME_API_FORMAT = "yyyy-MM-dd"
const val TIME_FORMAT = "HH:mm"
const val MILLIS = 1000

fun getFormatedTime(time: Long): String {
    return android.text.format.DateFormat.format(TIME_FORMAT, time * MILLIS).toString()
}

fun getDayOfYear(time: Long): Int {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = time * MILLIS
    return calendar.get(Calendar.DAY_OF_YEAR)
}

fun getDayName(time: Long): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = time * MILLIS
    return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US)
}

fun formatDate(year: Int, month: Int, dayOfMonth: Int): String {
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month)
    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
    val date = calendar.time
    val formatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault())
    return formatter.format(date)
}

fun formatDate(timestamp: String): String {
    return try {
        val simpleDateFormat = SimpleDateFormat(TIME_API_FORMAT, Locale.getDefault())
        val date = simpleDateFormat.parse(timestamp)
        val formatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault())
        formatter.format(date)
    } catch (e: Exception) {
        EMPTY_STRING
    }
}

/*
fun formatExpectedDate(timestamp: String): String {
    val simpleDateFormat = SimpleDateFormat(TIME_API_FORMAT, Locale.getDefault())
    val date = simpleDateFormat.parse(timestamp)
    val calendar = Calendar.getInstance()
    calendar.time = date
    return calendar.getDisplayName(Calendar.MONTH,
        Calendar.LONG, Locale.getDefault()) + SPACE + calendar.get(Calendar.DAY_OF_MONTH)
}
*/

fun formatToCalendar(timestamp: String): Calendar {
    val simpleDateFormat = SimpleDateFormat(TIME_API_FORMAT, Locale.getDefault())
    val date = simpleDateFormat.parse(timestamp)
    val calendar = Calendar.getInstance()
    calendar.time = date
    return calendar
}

fun formatToCalendar(year: Int, month: Int, dayOfMonth: Int): Calendar {
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month)
    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
    return calendar
}

fun formatTimeApi(calendar: Calendar?): String {
    var param: String = EMPTY_STRING
    calendar?.let {
        val simpleDateFormat = SimpleDateFormat(TIME_API_FORMAT, Locale.getDefault())
        param = simpleDateFormat.format(it.time)
    }
    return param
}

fun formatTimeApi(calendar: Calendar?, isSelected: Boolean): String {
    var param: String = EMPTY_STRING
    if (isSelected)
        calendar?.let {
            val simpleDateFormat = SimpleDateFormat(TIME_API_FORMAT, Locale.getDefault())
            param = simpleDateFormat.format(it.time)
        }
    return param
}

