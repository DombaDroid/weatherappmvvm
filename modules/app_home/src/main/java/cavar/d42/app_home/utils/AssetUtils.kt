package cavar.d42.app_home.utils

import cavar.d42.app_home.R
import cavar.d42.app_home.constants.*

fun prepareBackgroundImage(icon: String) : Int {
    return when (icon) {
        ICON_CLEAR_SKY_DAY -> R.drawable.background_clear_day
        ICON_FEW_CLOUDS_DAY, ICON_SCATTERED_CLOUDS_DAY, ICON_BROKEN_CLOUDS_DAY -> R.drawable.background_cloudly_day
        ICON_SHOWER_RAIN_DAY, ICON_RAIN_DAY -> R.drawable.background_rain_day
        ICON_THUNDER_STORM_DAY -> R.drawable.background_thunderstorm_day
        ICON_SNOW_DAY -> R.drawable.background_snow_day
        ICON_MIST_DAY -> R.drawable.background_foggy_day
        ICON_CLEAR_SKY_NIGHT -> R.drawable.background_clear_night
        ICON_FEW_CLOUDS_NIGHT, ICON_SCATTERED_CLOUDS_NIGHT, ICON_BROKEN_CLOUDS_NIGHT -> R.drawable.background_cloudly_night
        ICON_SHOWER_RAIN_NIGHT, ICON_RAIN_NIGHT, ICON_THUNDER_STORM_NIGHT -> R.drawable.background_rain_night
        ICON_SNOW_NIGHT -> R.drawable.background_snow_night
        else -> R.drawable.background_foggy_night
    }
}

fun prepareBackgroundColor(icon: String) : Int {
    return when (icon) {
        ICON_FEW_CLOUDS_DAY, ICON_SCATTERED_CLOUDS_DAY, ICON_BROKEN_CLOUDS_DAY, ICON_CLEAR_SKY_DAY, ICON_MIST_DAY -> R.drawable.gradient_sunny_day
        ICON_SHOWER_RAIN_DAY, ICON_RAIN_DAY, ICON_THUNDER_STORM_DAY -> R.drawable.gradient_rain_day
        ICON_SNOW_DAY -> R.drawable.gradient_snow_day
        else -> R.drawable.gradient_night
    }
}

fun prepareIcon(icon: String): Int {
    return when (icon) {
        ICON_CLEAR_SKY_DAY -> R.drawable.ic_sun
        ICON_FEW_CLOUDS_DAY, ICON_SCATTERED_CLOUDS_DAY, ICON_BROKEN_CLOUDS_DAY, ICON_FEW_CLOUDS_NIGHT, ICON_SCATTERED_CLOUDS_NIGHT, ICON_BROKEN_CLOUDS_NIGHT -> R.drawable.ic_cloud
        ICON_SHOWER_RAIN_DAY, ICON_RAIN_DAY, ICON_SHOWER_RAIN_NIGHT, ICON_RAIN_NIGHT, ICON_THUNDER_STORM_NIGHT -> R.drawable.ic_rain
        ICON_THUNDER_STORM_DAY -> R.drawable.ic_thunder
        ICON_SNOW_DAY, ICON_SNOW_NIGHT -> R.drawable.ic_snow
        ICON_MIST_DAY, ICON_MIST_NIGHT -> R.drawable.ic_fog
        else -> R.drawable.ic_moon
    }
}