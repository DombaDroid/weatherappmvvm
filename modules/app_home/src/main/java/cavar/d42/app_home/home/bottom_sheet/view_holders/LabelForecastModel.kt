package cavar.d42.app_home.home.bottom_sheet.view_holders

import android.view.View
import android.widget.TextView
import cavar.d42.app_home.R
import cavar.d42.papercut.constants.EMPTY_STRING
import com.airbnb.epoxy.*

@EpoxyModelClass
abstract class LabelForecastModel : EpoxyModelWithHolder<LabelForecastViewHolder>() {
    @EpoxyAttribute var label: String = EMPTY_STRING

    override fun bind(holder: LabelForecastViewHolder) {
        holder.tvLabel.text = label
    }

    override fun getDefaultLayout(): Int = R.layout.cell_label
}

class LabelForecastViewHolder : EpoxyHolder(){
    lateinit var tvLabel : TextView
    override fun bindView(itemView: View) {
        tvLabel = itemView.findViewById(R.id.tvLabel)
    }
}