package cavar.d42.app_home.mapper

import cavar.d42.api_common.models.WeatherCity
import cavar.d42.app_commons.extensions.capitaliseFirstLetter
import cavar.d42.app_commons.utils.getFormatedTime
import cavar.d42.app_home.home.ui.ForecastControllerData
import cavar.d42.app_home.home.ui.HomeUiData
import cavar.d42.app_home.utils.prepareBackgroundColor
import cavar.d42.app_home.utils.prepareBackgroundImage
import cavar.d42.app_home.utils.prepareIcon
import cavar.d42.papercut.constants.EMPTY_STRING

fun WeatherCity.toHomeUiData(): HomeUiData {
    return HomeUiData(
        Math.round(temp),
        weatherInfo,
        name ?: EMPTY_STRING,
        Math.round(tempMin),
        Math.round(tempMax),
        humidity,
        Math.round(wind),
        Math.round(pressure),
        weatherInfo,
        prepareBackgroundImage(icon),
        prepareBackgroundColor(icon)
    )
}

fun WeatherCity.toForecastControllerData(): ForecastControllerData {
    return ForecastControllerData(
        time,
        prepareIcon(icon),
        getFormatedTime(time),
        weatherInfo.capitaliseFirstLetter(),
        Math.round(temp)
    )
}