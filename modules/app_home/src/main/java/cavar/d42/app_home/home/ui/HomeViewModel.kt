package cavar.d42.app_home.home.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cavar.d42.api_common.data.RxWrapper
import cavar.d42.api_common.models.WeatherCity
import cavar.d42.app_api.constants.DEFAULT_CITY
import cavar.d42.app_commons.models.Coordinates
import cavar.d42.app_home.mapper.toHomeUiData
import cavar.d42.app_home.home.repository.HomeRepository
import cavar.d42.app_home.mapper.toForecastControllerData
import cavar.d42.papercut.custom_events.SingleLiveEvent
import cavar.d42.papercut.extensions.usualThreads
import cavar.d42.papercut.view_model.BaseErrorViewModel
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject

data class HomeUiData(
    val temp: Int,
    val weatherInfo: String,
    val name: String,
    val tempMin: Int,
    val tempMax: Int,
    val humidity: Int,
    val wind: Int,
    val pressure: Int,
    val icon: String,
    val backgroundDrawableId: Int,
    val backgroundColorId: Int
)

data class ForecastControllerData(
    val time: Long,
    val icon: Int,
    val formatTime: String,
    val info: String,
    val temp: Int
)

class HomeViewModel(private val repository: HomeRepository) : BaseErrorViewModel() {

    private val _showBottomSheet = SingleLiveEvent<Any>()
    val showBottomSheet: LiveData<Any>
        get() = _showBottomSheet

    private val _loadMapFragment = SingleLiveEvent<Any>()
    val loadMapFragment: LiveData<Any>
        get() = _loadMapFragment

    val homeUiData = MutableLiveData<HomeUiData>()
    val weatherList = MutableLiveData<List<ForecastControllerData>>()

    private val eventReload = PublishSubject.create<Boolean>()
    private val eventInit = PublishSubject.create<WeatherCity>()
    private val eventLocationChange = PublishSubject.create<Coordinates>()

    init {
        val initObserver = eventInit
            .map { RxWrapper(it)}

        val reloadObserver = eventReload
            .flatMap {
            repository.getWeatherByName(homeUiData.value?.name ?: DEFAULT_CITY)
        }

        val eventResult = eventLocationChange
            .flatMap { repository.getWeatherByLatLng(it.latitude, it.longitude) }

        addDisposable(
            Observable.merge(initObserver, reloadObserver, eventResult)
                .usualThreads()
                .withLoader()
                .subscribeBy(
                    onNext = {
                        it.value?.let {
                            homeUiData.value = it.toHomeUiData()
                            weatherList.value = it.list?.map { it.toForecastControllerData() }
                        }
                        handleError(it.error)
                    }
                )
        )
    }

    fun onInit(weatherCity: WeatherCity) {
        eventInit.onNext(weatherCity)
    }

    fun onLocationChange(latitude: Double, longitude: Double) {
        eventLocationChange.onNext(Coordinates(latitude, longitude))
    }

    override fun onRefresh() {
        eventReload.onNextWithLoader(true)
    }

    fun prepareBottomSheet() {
        _showBottomSheet.call()
    }

    fun onGpsClick() {
        _loadMapFragment.call()
    }
}