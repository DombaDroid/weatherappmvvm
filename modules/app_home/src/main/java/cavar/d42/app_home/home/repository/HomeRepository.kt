package cavar.d42.app_home.home.repository

import cavar.d42.api_common.data.RxWrapper
import cavar.d42.api_common.extensions.wrap
import cavar.d42.api_common.interactors.auto_complete.AutoCompleteInteractor
import cavar.d42.api_common.interactors.weather.WeatherInteractor
import cavar.d42.error_handler.ErrorHandler
import cavar.d42.error_handler.ErrorResult
import cavar.d42.error_handler.ErrorType
import io.reactivex.Observable

class HomeRepository(
    private val getWeatherInteractor: WeatherInteractor,
    private val getAutoCompleteInteractor: AutoCompleteInteractor,
    private val errorHandler: ErrorHandler
) {

    fun getWeatherByName(name: String) = getWeatherInteractor.getCurrentWeatherByCityName(name)
        .wrap()
        .onErrorReturn {
            RxWrapper(null, errorHandler.baseHttpErrorHandler(ErrorType.Snack, it))
        }

    fun getWeatherByLatLng(latitude: Double, longitude: Double) =
        getWeatherInteractor.getCurrentWeatherByCoordinates(latitude, longitude)
            .wrap()
            .onErrorReturn {
                RxWrapper(null, errorHandler.baseHttpErrorHandler(ErrorType.Snack, it))
            }
}