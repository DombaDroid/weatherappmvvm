package cavar.d42.app_home.home.bottom_sheet.controler

import cavar.d42.api_common.models.WeatherCity
import cavar.d42.app_commons.constants.INIT_INT
import cavar.d42.app_commons.utils.getDayName
import cavar.d42.app_commons.utils.getDayOfYear
import cavar.d42.app_home.home.bottom_sheet.WeatherForcastDelegate
import cavar.d42.app_home.home.bottom_sheet.view_holders.header
import cavar.d42.app_home.home.bottom_sheet.view_holders.labelForecast
import cavar.d42.app_home.home.bottom_sheet.view_holders.weatherForecast
import cavar.d42.app_home.home.ui.ForecastControllerData
import com.airbnb.epoxy.TypedEpoxyController

class WeatherForecastController(val listener: WeatherForcastDelegate) : TypedEpoxyController<List<ForecastControllerData>>() {

    override fun buildModels(data: List<ForecastControllerData>?) {
        var previousType = INIT_INT
        data?.let {
            header {
                id(-1)
                onClickListener { _, _, _, _ ->
                    listener.dismissBottomSheet()
                }
            }
            for (item in data) {
                if (getDayOfYear(item.time) != previousType) {
                    previousType = getDayOfYear(item.time)
                    labelForecast {
                        id(-item.time)
                        label(getDayName(item.time))
                    }
                }
                weatherForecast {
                    id(item.time)
                    iconId(item.icon)
                    time(item.formatTime)
                    description(item.info)
                    temp(item.temp)
                }
            }
        }

    }
}