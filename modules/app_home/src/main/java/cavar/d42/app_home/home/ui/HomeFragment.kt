package cavar.d42.app_home.home.ui

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import cavar.d42.app_commons.constants.*
import cavar.d42.app_commons.extensions.*
import cavar.d42.app_commons.router.Router
import cavar.d42.app_home.R
import cavar.d42.app_home.home.bottom_sheet.WeatherForecastBottomSheet
import cavar.d42.papercut.fragment.BaseErrorFragment
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject

class HomeFragment : BaseErrorFragment<HomeViewModel>(HomeViewModel::class) {

    companion object {
        fun newInstance(): Fragment {
            return HomeFragment()
        }
    }

    private val router: Router by inject()

    override val layoutRes: Int = R.layout.fragment_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setBackgroundDrawable(null)
        viewModel.onInit(arguments!!.getParcelable(EXTRAS_WEATHER))

        viewModel.showBottomSheet.observe(this, Observer {
            val fragment = WeatherForecastBottomSheet.newInstance()
            fragment.show(childFragmentManager, WeatherForecastBottomSheet.TAG)
        })

        viewModel.loadMapFragment.observe(this, Observer {
            loadFragmentFoResult(router.createMapFragment(), this)
        })

        viewModel.homeUiData.observe(this, Observer {
            tvCurrentTemp.formatText = getString(R.string.temp_degree, it.temp)
            tvCurrentStatus.formatText = it.weatherInfo.capitaliseFirstLetter()
            tvCity.formatText = it.name
            tvLow.formatText = getString(R.string.temp_degree, it.tempMin)
            tvHigh.formatText = getString(R.string.temp_degree, it.tempMax)
            tvHumidity.formatText = getString(R.string.temp_percentage, it.humidity)
            tvWindSpeed.formatText = getString(R.string.temp_kmh, it.wind)
            tvPressure.formatText = getString(R.string.temp_pressure, it.pressure)
            ivWeather.setImageSrc(it.backgroundDrawableId)
            clRoot.setBackgroundSrc(context!!, it.backgroundColorId)
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvCity.setOnClickListener { viewModel.onRefresh() }
        tvWeatherForecast.setOnClickListener { viewModel.prepareBottomSheet() }
        ivGps.setOnClickListener { viewModel.onGpsClick() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MAP_TO_HOME_RESULT && resultCode == RESULT_OK)
            data?.let {
                viewModel.onLocationChange(
                    it.getDoubleExtra(EXTRAS_LATITUDE, INVALID_COORDINATE),
                    it.getDoubleExtra(EXTRAS_LONGITUDE, INVALID_COORDINATE)
                )
            }
    }
}