package cavar.d42.app_home.home.bottom_sheet

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import cavar.d42.app_home.R
import cavar.d42.app_home.home.bottom_sheet.controler.WeatherForecastController
import cavar.d42.app_home.home.ui.HomeViewModel
import cavar.d42.papercut.bottom_sheet.BaseBottomSheet
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_weather_forecast.*

class WeatherForecastBottomSheet : BaseBottomSheet<HomeViewModel>(HomeViewModel::class), WeatherForcastDelegate {

    companion object {
        const val TAG = "WeatherForecastBottomSheet"

        fun newInstance(): BottomSheetDialogFragment {
            return WeatherForecastBottomSheet()
        }
    }

    val controler = WeatherForecastController(this)

    override val layoutRes: Int = R.layout.bottom_sheet_weather_forecast

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
        viewModel.weatherList.observe(this, Observer {
            controler.setData(it)
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        epRecyclerView.layoutManager = LinearLayoutManager(context)
        epRecyclerView.setController(controler)
    }

    override fun dismissBottomSheet() {
        dismiss()
    }
}

interface WeatherForcastDelegate {
    fun dismissBottomSheet()
}