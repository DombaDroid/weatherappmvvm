package cavar.d42.app_api.services.auto_complete

import cavar.d42.app_api.BuildConfig
import cavar.d42.app_api.constants.*
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

inline fun provideAuthInterceptor(): Interceptor {
    return Interceptor {
        val original = it.request()
        val originalHttpUrl = original.url()
        val url = originalHttpUrl.newBuilder()
            .addQueryParameter(KEY, PLACE_AUTOCOMPLETE_KEY)
            .addQueryParameter(TYPES, REGIONS)
            .build()
        it.proceed(original.newBuilder().url(url).build())
    }
}

inline fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    if (BuildConfig.DEBUG) {
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    } else {
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
    }
    return httpLoggingInterceptor
}

inline fun provideGson() = GsonBuilder().create()

inline fun provideConverterFactory() = GsonConverterFactory.create(cavar.d42.app_api.services.weather.provideGson())

inline fun provideCallFactory() = RxJava2CallAdapterFactory.create()

inline fun createOkHttpClient() =
    OkHttpClient.Builder()
        .addInterceptor(cavar.d42.app_api.services.weather.provideAuthInterceptor())
        .addInterceptor(cavar.d42.app_api.services.weather.provideLoggingInterceptor())
        .build()

inline fun <reified T> createAutoCompleteWebService(): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(PLACE_AUTOCOMPLETE_BASE_URL)
        .client(cavar.d42.app_api.services.weather.createOkHttpClient())
        .addConverterFactory(cavar.d42.app_api.services.weather.provideConverterFactory())
        .addCallAdapterFactory(cavar.d42.app_api.services.weather.provideCallFactory())
        .build()
    return retrofit.create(T::class.java)
}