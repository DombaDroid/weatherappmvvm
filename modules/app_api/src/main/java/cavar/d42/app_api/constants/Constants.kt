package cavar.d42.app_api.constants

//WEATHER REST
const val WEATHER_API_KEY = "2a8fc52212d1d020c4b3ac497469a6ef"
const val WEATHER_BASE_URL = "http://api.openweathermap.org/data/2.5/"
//AUTO COMPLETE
const val PLACE_AUTOCOMPLETE_BASE_URL = "https://maps.googleapis.com/maps/api/place/"
const val PLACE_AUTOCOMPLETE_KEY = "AIzaSyDvEkizmIsTbOdSklVl0pBFaCw7JRudfFg"

//Retrofit
const val APP_ID = "APPID"
const val REST_WEATHER = "REST_WEATHER"
const val REST_PLACE_AUTOCOMPLETE = "REST_PLACE_AUTOCOMPLETE"
const val UNITS = "units"
const val METRIC = "metric"
const val TYPES = "types"
const val REGIONS = "(regions)"
const val KEY = "key"
const val DEFAULT_CITY = "Osijek"

//Error Types
const val TYPE_SERVER_ERROR = 0
const val TYPE_NETWORK_ERROR = 1

//Location
val DEFAULT_CITY_LATITUDE: Double = 45.553551
val DEFAULT_CITY_LONGITUDE: Double = 18.694536

//PERMISSIONS
val REQUEST_FINE_LOCATION = 104