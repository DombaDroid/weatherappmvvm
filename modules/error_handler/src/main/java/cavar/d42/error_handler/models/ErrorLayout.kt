package cavar.d42.error_handler.models

import cavar.d42.error_handler.RefreshListener

data class ErrorLayout(
    val title: Int,
    val description: Int,
    val buttonText: Int,
    val imageSrc: Int,
    var listener: RefreshListener? = null
)