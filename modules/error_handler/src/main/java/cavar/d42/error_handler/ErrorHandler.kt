package cavar.d42.error_handler

import android.content.res.Resources
import cavar.d42.error_handler.constants.EMPTY_STRING
import cavar.d42.error_handler.models.ErrorLayout
import cavar.d42.error_handler.models.ErrorSnack
import java.net.InetAddress

class ErrorHandler(val resources: Resources) {

    fun customHttpErrorHandler(it: Throwable) : String {
        return EMPTY_STRING
    }

    fun baseHttpErrorHandler(type: ErrorType = ErrorType.NoOp, it: Throwable? = null): ErrorResult {
        return when (type) {
            ErrorType.Snack -> {
                if(!isInternetAvailable()){
                    ErrorResult.ErrorSnackData(ErrorSnack(R.string.error_server_description))
                } else {
                    ErrorResult.ErrorSnackData(ErrorSnack(R.string.error_internet_description))
                }
            }
            ErrorType.Layout -> {
                if(isInternetAvailable()){
                    ErrorResult.ErrorLayoutData(ErrorLayout(   R.string.error_server_title, R.string.error_server_description,
                      R.string.try_again, R.drawable.ic_error_server))
                } else {
                    ErrorResult.ErrorLayoutData(ErrorLayout(   R.string.error_internet_title, R.string.error_internet_description,
                        R.string.try_again, R.drawable.ic_error_network))
                }
            }
            else -> ErrorResult.ErrorNoOp
        }
    }

    fun isInternetAvailable(): Boolean {
        try {
            val ipAddr = InetAddress.getByName("google.com")
            //You can replace it with your name
            return ipAddr.toString() != ""

        } catch (e: Exception) {
            return false
        }
    }
}

sealed class ErrorResult {
    data class ErrorSnackData(val data: ErrorSnack) : ErrorResult()
    data class ErrorLayoutData(val data: ErrorLayout) : ErrorResult()
    object ErrorNoOp : ErrorResult()
}

sealed class ErrorType {
    object Snack : ErrorType()
    object Layout : ErrorType()
    object NoOp : ErrorType()
}

interface RefreshListener {
    fun onRefresh()
}
