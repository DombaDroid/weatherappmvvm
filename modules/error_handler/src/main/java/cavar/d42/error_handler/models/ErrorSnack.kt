package cavar.d42.error_handler.models

import cavar.d42.error_handler.R
import cavar.d42.error_handler.constants.EMPTY_STRING

data class ErrorSnack(var string: Any = EMPTY_STRING, val colorId: Int = R.color.red)