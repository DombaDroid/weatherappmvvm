package cavar.d42.app_map.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import cavar.d42.app_api.constants.REQUEST_FINE_LOCATION
import cavar.d42.app_commons.constants.EXTRAS_LATITUDE
import cavar.d42.app_commons.constants.EXTRAS_LONGITUDE
import cavar.d42.app_commons.extensions.returnFragmentForResult
import cavar.d42.app_map.R
import cavar.d42.papercut.fragment.BaseFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_map.*

class MapFragment : BaseFragment<MapViewModel>(MapViewModel::class), OnMapReadyCallback, GoogleMap.OnMapClickListener {

    companion object {
        const val TAG = "MapFragment"

        fun newInstance(): Fragment {
            return MapFragment()
        }
    }

    private lateinit var mapFragment: SupportMapFragment
    private var googleMap: GoogleMap? = null
    var marker: Marker? = null

    override val layoutRes: Int = R.layout.fragment_map

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.getAsyncMap.observe(this, Observer {
            getAsyncMap()
        })

        viewModel.checkPermission.observe(this, Observer {
            checkPermissions()
        })

        viewModel.moveCamera.observe(this, Observer {
            it?.let { latLng ->
                moveCamera(latLng)
            }
        })

        viewModel.mapUiData.observe(this, Observer {
            it.pin?.let { latLng ->
                marker?.remove()
                marker = addMarker(latLng)
                moveCamera(latLng)
            }
            if (it.isFabVisible)
                fab.show()
            else
                fab.hide()
        })

        viewModel.returnResult.observe(this, Observer { latLng ->
            val intent = Intent()
            intent.putExtra(EXTRAS_LATITUDE, latLng.latitude)
            intent.putExtra(EXTRAS_LONGITUDE, latLng.longitude)
            returnFragmentForResult(intent)
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener { viewModel.onFabClick() }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        styleMap()
        googleMap.setOnMapClickListener(this)
        viewModel.onMapReady()
    }

    override fun onMapClick(latLng: LatLng) {
        viewModel.onMapClick(latLng)
    }

    fun styleMap() {
        googleMap?.uiSettings?.isCompassEnabled = false
        val locationButton =
            (mapFragment.view?.findViewById<View>(Integer.parseInt("1"))?.parent as View).findViewById<View>(
                Integer.parseInt("2")
            )
        val rlp = locationButton.layoutParams as RelativeLayout.LayoutParams
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
        rlp.setMargins(0, resources.getDimension(R.dimen.dp41).toInt(), resources.getDimension(R.dimen.dp16).toInt(), 0)
    }

    private fun moveCamera(latLng: LatLng) {
        googleMap?.animateCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    private fun addMarker(latLng: LatLng): Marker? {
        return googleMap?.addMarker(
            MarkerOptions()
                .position(latLng)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin))
        )
    }

    private fun getAsyncMap() {
        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            )
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_FINE_LOCATION)
            else
                viewModel.permissionSuccess()
        } else {
            viewModel.permissionSuccess()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_FINE_LOCATION && permissions.isNotEmpty())
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                viewModel.permissionSuccess()
            else
                viewModel.permissionDenied()
    }
}