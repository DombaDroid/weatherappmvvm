package cavar.d42.app_map.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cavar.d42.app_map.R
import cavar.d42.app_map.repository.MapRepository
import cavar.d42.papercut.custom_events.SingleLiveEvent
import cavar.d42.papercut.fragment.SnackData
import cavar.d42.papercut.view_model.BaseViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import io.reactivex.rxkotlin.subscribeBy

data class MapUiData(var isFabVisible: Boolean, var pin: LatLng?)

class MapViewModel(private val repository: MapRepository) : BaseViewModel() {

    private val _getAsyncMap = SingleLiveEvent<Any>()
    val getAsyncMap: LiveData<Any>
        get() = _getAsyncMap

    private val _checkPermission = SingleLiveEvent<Any>()
    val checkPermission: LiveData<Any>
        get() = _checkPermission

    private val _moveCamera = SingleLiveEvent<LatLng>()
    val moveCamera: LiveData<LatLng>
        get() = _moveCamera

    private val _returnResult = SingleLiveEvent<LatLng>()
    val returnResult: LiveData<LatLng>
        get() = _returnResult

    val mapUiData = MutableLiveData<MapUiData>(MapUiData(false, null))

    init {
        _getAsyncMap.call()
    }

    fun permissionSuccess() {
        addDisposable(repository.getLocationInteractor()
            .subscribeBy(
                onNext = { _moveCamera.value = LatLng(it.latitude, it.longitude) },
                onError = { it.printStackTrace() }
            ))
    }

    fun permissionDenied() {
        _showSnack.value = SnackData(R.string.gps_error, R.color.gray_gun_metal)
    }

    fun onMapClick(latLng: LatLng) {
        mapUiData.value = MapUiData(true, latLng)
    }

    fun onMapReady() {
        _checkPermission.call()
    }

    fun onFabClick() {
        _returnResult.value = mapUiData.value!!.pin
    }
}