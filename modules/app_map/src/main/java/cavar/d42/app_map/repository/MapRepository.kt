package cavar.d42.app_map.repository

import android.location.Location
import cavar.d42.api_common.interactors.location.LocationInteractor
import cavar.d42.papercut.extensions.usualThreads
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class MapRepository(private val getLocationInteractor: LocationInteractor) {

    fun getLocationInteractor() : Observable<Location> {
        return getLocationInteractor.getLatestLocations()
            .take(1)
            .timeout(7, TimeUnit.SECONDS)
            .usualThreads()
    }
}