package cavar.d42.api_common.interactors.location

import android.annotation.SuppressLint
import android.location.Location
import cavar.d42.api_common.interactors.location.LocationInteractor
import com.google.android.gms.location.LocationRequest
import com.patloew.rxlocation.RxLocation
import io.reactivex.Observable

class LocationInteractorImpl constructor(val locationRequest: LocationRequest, val rxLocation: RxLocation):
    LocationInteractor {
    @SuppressLint("MissingPermission")
    override fun getLatestLocations(): Observable<Location> {
        return rxLocation.location().updates(locationRequest)
    }
}