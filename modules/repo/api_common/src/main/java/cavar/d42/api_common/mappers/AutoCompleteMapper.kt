package cavar.d42.api_common.mappers

import cavar.d42.api_common.models.Predictions
import cavar.d42.api_common.response.AutoCompleteEntity

fun AutoCompleteEntity.toPredictionList(): List<Predictions>{
    return predictionsEntity.map { Predictions(it.descriptionEntity, it.termsEntity[0].city) }
}