package cavar.d42.api_common.response

import com.google.gson.annotations.SerializedName

data class WeatherItemEntity(
    @SerializedName("id") val id: Int,
    @SerializedName("main") val main: String,
    @SerializedName("description") val description: String,
    @SerializedName("icon") val icon: String
)