package cavar.d42.api_common.interactors.auto_complete

import cavar.d42.api_common.AutoCompleteRestInterface
import cavar.d42.api_common.interactors.auto_complete.AutoCompleteInteractor
import cavar.d42.api_common.mappers.toPredictionList
import cavar.d42.api_common.models.Predictions
import cavar.d42.api_common.response.AutoCompleteEntity
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class AutoCompleteInteractorImpl constructor(val autoCompleteRestInterface: AutoCompleteRestInterface) :
    AutoCompleteInteractor {

    override fun getAutoCompleteList(word: String): Observable<List<Predictions>> {
        return autoCompleteRestInterface.getAutocompletedCities(word)
            .flatMapObservable { autoComplete: AutoCompleteEntity ->  Observable.just(autoComplete.toPredictionList())}
            .subscribeOn(Schedulers.io())
    }
}