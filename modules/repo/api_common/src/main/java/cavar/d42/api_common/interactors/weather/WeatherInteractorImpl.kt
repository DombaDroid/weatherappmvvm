package cavar.d42.api_common.interactors.weather

import cavar.d42.api_common.WeatherRestInterface
import cavar.d42.api_common.mappers.toWeatherCity
import cavar.d42.api_common.models.WeatherCity
import cavar.d42.api_common.response.CurrentWeatherEntity
import cavar.d42.api_common.response.WeatherForecastEntity
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class WeatherInteractorImpl constructor(val weatherRestInterface: WeatherRestInterface) : WeatherInteractor {
    override fun getCurrentWeatherByCoordinates(latitude: Double, longitude: Double): Observable<WeatherCity> {
        return weatherRestInterface.getCurrentWeatherByCoordinates(latitude, longitude).toObservable()
            .flatMap {
                Observable.zip(
                    Observable.just(it),
                    weatherRestInterface.getWeatherForecast(it.name!!).toObservable(),
                    BiFunction<CurrentWeatherEntity, WeatherForecastEntity, WeatherCity> { currentWeather, weatherForecast ->
                        currentWeather.toWeatherCity(weatherForecast)
                    })
            }.subscribeOn(Schedulers.io())
    }

    override fun getCurrentWeatherByCityName(city: String): Observable<WeatherCity> {
        return Observable.zip(weatherRestInterface.getCurrentWeatherByCityName(city).toObservable(),
            weatherRestInterface.getWeatherForecast(city).toObservable(),
            BiFunction<CurrentWeatherEntity, WeatherForecastEntity, WeatherCity> { currentWeather, weatherForecast ->
                currentWeather.toWeatherCity(weatherForecast)
            }).subscribeOn(Schedulers.io())
    }
}