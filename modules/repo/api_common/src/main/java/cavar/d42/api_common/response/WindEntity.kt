package cavar.d42.api_common.response

import com.google.gson.annotations.SerializedName

data class WindEntity(@SerializedName("speed") val speed: Float) {
}