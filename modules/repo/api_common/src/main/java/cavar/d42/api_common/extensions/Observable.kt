package cavar.d42.api_common.extensions

import cavar.d42.api_common.data.RxWrapper
import cavar.d42.error_handler.ErrorResult
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers


fun <T> Observable<T>.wrap(): Observable<RxWrapper<T>> {
    return map { RxWrapper(it) }
}