package cavar.d42.api_common.interactors.auto_complete

import cavar.d42.api_common.models.Predictions
import io.reactivex.Observable

interface AutoCompleteInteractor {
    fun getAutoCompleteList(word: String) : Observable<List<Predictions>>
}