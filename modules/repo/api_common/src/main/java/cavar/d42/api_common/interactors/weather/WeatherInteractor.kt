package cavar.d42.api_common.interactors.weather

import cavar.d42.api_common.models.WeatherCity
import io.reactivex.Observable
import io.reactivex.Single

interface WeatherInteractor {
    fun getCurrentWeatherByCoordinates(latitude: Double, longitude: Double) : Observable<WeatherCity>
    fun getCurrentWeatherByCityName(city: String) : Observable<WeatherCity>
}