package cavar.d42.api_common.response

import cavar.d42.api_common.response.CurrentWeatherEntity
import cavar.d42.api_common.response.WeatherForecastEntity

class ZipWeatherWrapperEntity (val weather: CurrentWeatherEntity, val weatherForecast: WeatherForecastEntity) {
}