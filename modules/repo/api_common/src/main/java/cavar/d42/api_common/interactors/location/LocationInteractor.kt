package cavar.d42.api_common.interactors.location

import android.location.Location
import io.reactivex.Observable

interface LocationInteractor {
    fun getLatestLocations(): Observable<Location>
}