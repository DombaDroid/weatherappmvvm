package cavar.d42.api_common.data

import cavar.d42.error_handler.ErrorResult

data class RxWrapper<out T>(val value: T? = null, val error: ErrorResult = ErrorResult.ErrorNoOp)