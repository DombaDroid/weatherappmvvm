package cavar.d42.api_common

import cavar.d42.api_common.response.AutoCompleteEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface AutoCompleteRestInterface {
    @GET("autocomplete/json")
    fun getAutocompletedCities(@Query("input") word: String): Single<AutoCompleteEntity>
}