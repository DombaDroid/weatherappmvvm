package cavar.d42.api_common.response

import cavar.d42.api_common.response.CurrentWeatherEntity
import com.google.gson.annotations.SerializedName

data class WeatherForecastEntity (@SerializedName("list") val list: List<CurrentWeatherEntity>)