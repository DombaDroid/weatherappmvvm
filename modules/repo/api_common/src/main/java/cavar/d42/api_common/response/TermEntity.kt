package cavar.d42.api_common.response

import com.google.gson.annotations.SerializedName

data class TermEntity (@SerializedName("value") val city: String) {
}