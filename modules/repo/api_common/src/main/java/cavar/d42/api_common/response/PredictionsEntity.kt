package cavar.d42.api_common.response

import com.google.gson.annotations.SerializedName

data class PredictionsEntity(@SerializedName("description") val descriptionEntity: String,
                        @SerializedName("terms") val termsEntity: List<TermEntity>)